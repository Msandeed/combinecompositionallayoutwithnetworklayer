//
//  ApiService.swift
//  TunesWithCombine
//
//  Created by Mostafa Sandeed on 3/29/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation
import Combine

enum ApiError: Error {
  case parsing(description: String)
  case network(description: String)
}

class ApiService {
    
    static let shared = ApiService()
    
    func getTopAlbums() -> AnyPublisher<Topalbums, ApiError> {
        
        guard let url = makeTopAlbumsComponents(withArtist: "The Cranberries").url else {
          let error = ApiError.network(description: "Couldn't create URL")
          return Fail(error: error).eraseToAnyPublisher()
        }
        
        return URLSession.shared.dataTaskPublisher(for: url)
            .map { $0.data }    // Extract data from response
            .decode(type: TopAlbumsResponse.self, decoder: JSONDecoder())     // Decode data to model
            .map { $0.topalbums }    // Extract TopAlbums property from TopAlbumsResponse
            .mapError { error in
              .parsing(description: error.localizedDescription)     // Map error to custom error
            }
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
}

extension ApiService {
    
    struct API {
      static let scheme = "http"
      static let host = "ws.audioscrobbler.com"
      static let path = "/2.0/"
      static let key = "1b55539b76641b65dd15dc411858f4ff"
    }

    func makeTopAlbumsComponents(withArtist artist: String) -> URLComponents {
      var components = URLComponents()
      components.scheme = API.scheme
      components.host = API.host
      components.path = API.path
      
      components.queryItems = [
        URLQueryItem(name: "method", value: "artist.gettopalbums"),
        URLQueryItem(name: "format", value: "json"),
        URLQueryItem(name: "artist", value: artist),
        URLQueryItem(name: "api_key", value: API.key)
      ]
      
      return components
    }
    
}

