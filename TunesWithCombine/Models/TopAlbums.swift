//
//  TopAlbums.swift
//  TunesWithCombine
//
//  Created by Mostafa Sandeed on 3/29/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

// MARK: - TopAlbumsResponse
class TopAlbumsResponse: Codable {
    let topalbums: Topalbums

    init(topalbums: Topalbums) {
        self.topalbums = topalbums
    }
}

// MARK: - Topalbums
class Topalbums: Codable {
    let album: [Album]
    let attr: Attr

    enum CodingKeys: String, CodingKey {
        case album
        case attr = "@attr"
    }

    init(album: [Album], attr: Attr) {
        self.album = album
        self.attr = attr
    }
}

// MARK: - Album
class Album: Codable {
    let name: String
    let playcount: Int
    let mbid: String?
    let url: String
    let artist: ArtistClass
    let image: [Image]

    init(name: String, playcount: Int, mbid: String?, url: String, artist: ArtistClass, image: [Image]) {
        self.name = name
        self.playcount = playcount
        self.mbid = mbid
        self.url = url
        self.artist = artist
        self.image = image
    }
}

// MARK: - ArtistClass
class ArtistClass: Codable {
    let name: String
    let mbid: String
    let url: String

    init(name: String, mbid: String, url: String) {
        self.name = name
        self.mbid = mbid
        self.url = url
    }
}

// MARK: - Image
class Image: Codable {
    let text: String
    let size: Size

    enum CodingKeys: String, CodingKey {
        case text = "#text"
        case size
    }

    init(text: String, size: Size) {
        self.text = text
        self.size = size
    }
}

enum Size: String, Codable {
    case extralarge = "extralarge"
    case large = "large"
    case medium = "medium"
    case small = "small"
}

// MARK: - Attr
class Attr: Codable {
    let artist: String
    let page, perPage, totalPages, total: String

    init(artist: String, page: String, perPage: String, totalPages: String, total: String) {
        self.artist = artist
        self.page = page
        self.perPage = perPage
        self.totalPages = totalPages
        self.total = total
    }
}
