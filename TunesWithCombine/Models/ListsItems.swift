//
//  FavoritesListItem.swift
//  TunesWithCombine
//
//  Created by Mostafa Sandeed on 3/31/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation

struct FavoritesListItem: Hashable {
 
    let mbid: String?
    let name: String?
    let playcount: Int
    let image: [Image]
    
    init(album: Album) {
        mbid = album.mbid
        name = album.name
        playcount = album.playcount
        image = album.image
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(identifier)
    }

    static func == (lhs: FavoritesListItem, rhs: FavoritesListItem) -> Bool {
        return lhs.identifier == rhs.identifier
    }

    private let identifier = UUID()
}
