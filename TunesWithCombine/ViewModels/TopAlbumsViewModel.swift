//
//  TopAlbumsViewModel.swift
//  TunesWithCombine
//
//  Created by Mostafa Sandeed on 3/29/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import Foundation
import Combine

class TopAlbumsViewModel: ObservableObject {
    
    @Published private(set) var state = State.idle
    private var bag = Set<AnyCancellable>()
    private let input = PassthroughSubject<Event, Never>()

    init() {
        
        Publishers.system(
            initial: state,
            reduce: Self.reduce,
            scheduler: RunLoop.main,
            feedbacks: [
                Self.whenLoading(),
                Self.userInput(input: input.eraseToAnyPublisher())
            ]
        )
        .assign(to: \.state, on: self)
        .store(in: &bag)
    
    }

    deinit {
        bag.removeAll()
    }
    
    func send(event: Event) {
        input.send(event)
    }
}

// MARK: - Inner Types

extension TopAlbumsViewModel {
    enum State {
        case idle
        case loading
        case loaded([FavoritesListItem])
        case error(Error)
    }
    
    enum Event {
        case onAppear
        case onSelectAlbum(Int)
        case onAlbumsLoaded([FavoritesListItem])
        case onFailedToLoadAlbums(Error)
    }
}

// MARK: - State Machine

extension TopAlbumsViewModel {
    static func reduce(_ state: State, _ event: Event) -> State {
        switch state {
        case .idle:
            switch event {
            case .onAppear:
                return .loading
            default:
                return state
            }
        case .loading:
            switch event {
            case .onFailedToLoadAlbums(let error):
                return .error(error)
            case .onAlbumsLoaded(let albums):
                return .loaded(albums)
            default:
                return state
            }
        case .loaded:
            return state
        case .error:
            return state
        }
    }
    
    static func whenLoading() -> Feedback<State, Event> {
        Feedback { (state: State) -> AnyPublisher<Event, Never> in
            guard case .loading = state else { return Empty().eraseToAnyPublisher() }
            
            return ApiService.shared.getTopAlbums()
                .map { $0.album.map(FavoritesListItem.init) }
                .map(Event.onAlbumsLoaded)
                .catch { Just(Event.onFailedToLoadAlbums($0)) }
                .eraseToAnyPublisher()
        }
    }
    
    static func userInput(input: AnyPublisher<Event, Never>) -> Feedback<State, Event> {
        Feedback { _ in input }
    }
}
