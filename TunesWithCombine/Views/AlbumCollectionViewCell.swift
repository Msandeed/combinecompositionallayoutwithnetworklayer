//
//  AlbumCollectionViewCell.swift
//  TunesWithCombine
//
//  Created by Mostafa Sandeed on 3/31/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireImage

class AlbumCollectionViewCell: UICollectionViewCell {
    static let reuseIdentifer = "AlbumCollectionViewCell"
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var albumArtImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func populate(with favoriteListItem: FavoritesListItem) {
        
        self.nameLabel.text = favoriteListItem.name
        
        let imageUrl = favoriteListItem.image[3].text
            
        DispatchQueue.global(qos: .userInteractive).async {
            Alamofire.request(imageUrl).responseImage { response in
                if let image = response.result.value {
                    DispatchQueue.main.async {
                        self.albumArtImageView.image = image
                    }
                }
            }
        }
    }

}
