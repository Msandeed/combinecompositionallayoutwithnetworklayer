//
//  TopAlbumsViewController.swift
//  TunesWithCombine
//
//  Created by Mostafa Sandeed on 3/29/20.
//  Copyright © 2020 MostafaSandeed. All rights reserved.
//

import UIKit
import Combine

enum Section: String, CaseIterable {
    case mostPlayed = "Most played"
    case other = "Other"
}

fileprivate typealias FavoritesDataSource = UICollectionViewDiffableDataSource<Section, FavoritesListItem>
fileprivate typealias FavoritesSnapshot = NSDiffableDataSourceSnapshot<Section, FavoritesListItem>

class TopAlbumsViewController: BaseViewController {

    static let sectionHeaderElementKind = "section-header-element-kind"
    
    @IBOutlet weak var favoritesCollectionView: UICollectionView!
    
    fileprivate var dataSource: FavoritesDataSource!
    
    let viewModel = TopAlbumsViewModel()
    private var stateSubscriber:AnyCancellable?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "The Cranberries"
        
        // CollectionView
        configureHierarchy()
        setupDataSource()
        
        // ViewModel & Binding
        subscribeToState()
        viewModel.send(event: .onAppear)
    }
    
// MARK: CollectionView basic configuration
    private func configureHierarchy() {
        
        favoritesCollectionView.register(UINib(nibName: AlbumCollectionViewCell.reuseIdentifer, bundle: nil), forCellWithReuseIdentifier: AlbumCollectionViewCell.reuseIdentifer)
        
        // For Section header
        favoritesCollectionView.register(HeaderView.self,
        forSupplementaryViewOfKind: TopAlbumsViewController.sectionHeaderElementKind,
        withReuseIdentifier: HeaderView.reuseIdentifier)
        
        favoritesCollectionView.collectionViewLayout = createLayout()
    }
    
// MARK: Binding
    func subscribeToState() {
        stateSubscriber = viewModel.$state
        .receive(on: DispatchQueue.main)
        .sink(receiveValue: { [weak self] value in
            switch value {
              case .idle:
                self?.hideActivityIndicator()
              case .loading:
                self?.showActivityIndicator()
              case .error(let error):
                self?.hideActivityIndicator()
                self?.showNotificationAlert(title: "Error", message: error.localizedDescription)
              case .loaded(let albums):
                self?.hideActivityIndicator()
                self?.createSnapshot(from: albums)
            }
        })
    }
}

// MARK: CollectionView delegate methods
extension TopAlbumsViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
    }
}

// MARK: DataSource
extension TopAlbumsViewController {
    
    func setupDataSource() {
        
        dataSource = FavoritesDataSource(collectionView: favoritesCollectionView) {
            (collectionView: UICollectionView, indexPath: IndexPath, item: FavoritesListItem) -> UICollectionViewCell? in
            
            guard let cell = collectionView.dequeueReusableCell(
              withReuseIdentifier: AlbumCollectionViewCell.reuseIdentifer,
              for: indexPath) as? AlbumCollectionViewCell else { fatalError("Could not create new cell") }
            cell.populate(with: item)
            return cell
        }
        
        // For section header
        dataSource.supplementaryViewProvider = { (
          collectionView: UICollectionView,
          kind: String,
          indexPath: IndexPath) -> UICollectionReusableView? in

          guard let supplementaryView = collectionView.dequeueReusableSupplementaryView(
            ofKind: kind,
            withReuseIdentifier: HeaderView.reuseIdentifier,
            for: indexPath) as? HeaderView else { fatalError("Cannot create header view") }

          supplementaryView.label.text = Section.allCases[indexPath.section].rawValue
          return supplementaryView
        }
        
    }
    
    func createSnapshot(from favorites: [FavoritesListItem]) {
        
        var snapshot = FavoritesSnapshot()
        
        snapshot.appendSections([.mostPlayed, .other])
        let mostPlayed = favorites.filter { $0.playcount > 100000 }
        let other = favorites.filter { $0.playcount < 100000 }
        snapshot.appendItems(mostPlayed, toSection: .mostPlayed)
        snapshot.appendItems(other, toSection: .other)
        
        dataSource.apply(snapshot, animatingDifferences: true)
    }
}

